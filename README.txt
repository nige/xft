This package contain a X-ray Reflectometry fitting tool (XFT)

Support
=======
Tutorials can be found at: 
If you need more support send an e-mail to nige@space.dtu.dk

Initiate
========
Using spyder:
Make sure plotting functions are correct: Preferences -> IPython console -> Graphics -> Graphics backend = Automatic.
Call run main.py

References
==========
If you use the program please give reference to the following publication:


Changes 
==============
 *